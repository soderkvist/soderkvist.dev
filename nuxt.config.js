import whitelister from 'purgecss-whitelister'

export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Niclas Söderkvist - Web Developer',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Roboto&family=Roboto+Mono&display=swap"' }
    ],
    bodyAttrs: {
      class: 'bg-gray-800 font-body'
    }

  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/custom.scss',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '@/plugins/sal.js', ssr: false },
    /* { src: '@/plugins/sal', ssr: false }, */
    { src: '@/plugins/smooth-polyfill.js', ssr: false },
    { src: '@/plugins/vue-lazyload.js', ssr: false }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    ['@nuxtjs/google-gtag', {
      id: 'UA-168140381-1',
      config: {
        anonymize_ip: true,
        send_page_view: false,
        linker: {
          domains: ['soderkvist.dev']
        }
      },
      debug: true,
      disableAutoPageTrack: false,
    }],

    ['nuxt-fontawesome', {
      // component: 'fa',
      imports: [
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['fas']
        },
        {
          set: '@fortawesome/free-brands-svg-icons',
          icons: ['fab']
        }
      ]
    }]
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend () {}
  },

  purgeCSS: {
    whitelist: () => whitelister([
      './node_modules/@fortawesome/fontawesome-svg-core/styles.css',
      './node_modules/sal.js/dist/sal.css'
    ])
  }
}
