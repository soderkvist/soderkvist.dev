# soderkvist.dev

[![Netlify Status](https://api.netlify.com/api/v1/badges/86da6911-c042-441c-9283-f6cbec9ac796/deploy-status)](https://app.netlify.com/sites/youthful-heisenberg-37a2d7/deploys)

> Niclas Söderkvist dev site

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
