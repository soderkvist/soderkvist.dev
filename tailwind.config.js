/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
module.exports = {
  theme: {
    fontFamily: {
      display: ['Roboto Sans', 'sans-serif'],
      body: ['Roboto Sans', 'sans-serif'],
      mono: ['Roboto Mono', 'monospace'],
      sans: ['Roboto Sans', 'sans-serif'],
      serif: ['Roboto Mono', 'monospace']
    },
    extend: {
      colors: {
        gray: {
          100: '#A5A9B3', // '#f5f5f5',
          200: '#eeeeee',
          300: '#e0e0e0',
          400: '#bdbdbd',
          500: '#9e9e9e',
          600: '#757575',
          700: '#202123',
          800: '#292A2D', // '#424242',
          900: '#252627'// '#212121',
        }
      }
    }
  },
  variants: {},
  plugins: []
}
